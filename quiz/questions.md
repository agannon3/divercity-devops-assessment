## **Question 1:**

What is the principle of least privilege? What problems does it solve?

## **Question 2:**

A Senior Developer is hammering you for admin access to AWS resources, the Developer is not very AWS proficient and could very easily mess things up, what do you do?

## **Question 3:**

The company gets JSON files filled with some important data every week.
You are tasked to upload the contents of the JSON files to a database on AWS every week, the JSON data does not have a uniform schema.
Which database would you use?
Would you have to set up an EC2 instance to install said database?
Which other AWS service(s) would you use?
How would you automate this weekly task? 
What are the cost implications of your decisions?

## **Question 4:**

The company needs to store data in Amazon S3. A compliance requirement states that when any changes are made to objects, the previous state of the object with any changes must be preserved. Additionally, files older than 5 years should not be accessed but need to be archived for auditing.
What is the most cost-effective way to achieve this?